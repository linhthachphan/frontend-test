const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  entry: './index.ts',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: /node_modules/,
      loader: 'ts-loader',
      options: {
        appendTsSuffixTo: [/\.vue$/]
      }
    },
    {
      test: /\.vue$/,
      exclude: /node_modules/,
      loader: 'vue-loader'
    },
    {
      test: /\.scss$/,
      use: [
        'vue-style-loader',
        {
          loader: 'css-loader',
          options: {
            esModule: false
          }
        },
        'sass-loader'
      ]
    },
    {
      test: /\.(jpg|png)$/i,
      loader: 'file-loader',
      options: {
        outputPath: 'assets/images'
      }
    }]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'dist/index.html'),
      template: './index.html'
    }),
    new VueLoaderPlugin()

  ],
  resolve: {
    extensions: ['.js', '.vue', '.json', '.ts']
  },
  optimization: {
    minimize: false
  }
}
