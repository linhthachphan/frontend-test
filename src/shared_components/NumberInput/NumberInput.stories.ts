import NumberInput from './NumberInput.vue'

export default {
  title: 'Controls/Number Input',
  argTypes: {
    minValue: {
      description: 'minimum value of the input',
      table: {
        defaultValue: { summary: 0 }
      }
    },
    isReadOnly: {
      description: 'is the input non editable',
      table: {
        defaultValue: { summary: false }
      }
    },
    numberValueProp: {
      description: 'initial value'
    }

  }
}
const NumberInputTemplate = (args: any, { argTypes }: any) => ({
  components: { NumberInput },
  props: Object.keys(argTypes),
  template: '<NumberInput :minValue="minValue" :isReadOnly="isReadOnly"/>'
})

const Primary: any = NumberInputTemplate.bind({})

Primary.args = {
  minValue: 0,
  isReadOnly: false,
  numberValueProp: 4
}

export {
  Primary
}
