import { shallowMount } from '@vue/test-utils'
import NumberInput from './NumberInput.vue'

describe('number changed event', () => {
  it('should trigger the number-changed event', () => {
    const wrapper = shallowMount(NumberInput)
    wrapper.setValue(34)
    wrapper.trigger('change')
    expect(Object.keys(wrapper.emitted())).toContain('number-changed')
  })
  it('should emit empty value event', () => {
    const wrapper = shallowMount(NumberInput)
    wrapper.setValue('  ')
    wrapper.trigger('change')
    expect(Object.keys(wrapper.emitted())).toContain('invalid-value')
  })

  it('should emit invalid-value event', () => {
    const wrapper = shallowMount(NumberInput)
    wrapper.setValue('sdf')
    wrapper.trigger('change')
    expect(Object.keys(wrapper.emitted())).toContain('invalid-value')
  })

  it('should emit invalid-value event', () => {
    const wrapper = shallowMount(NumberInput, {
      propsData: {
        minValue: 0
      }
    })
    wrapper.setValue('-10')
    wrapper.trigger('change')
    expect(
      Object.keys(wrapper.emitted())
    ).toContain('smaller-than-min-value')
  })

  it('should emit number-changed with the correct value', () => {
    const wrapper = shallowMount(NumberInput)
    const value = 34
    wrapper.setValue(value)
    wrapper.trigger('change')
    const recievedValue = wrapper.emitted()['number-changed']
    expect(recievedValue).toEqual([['34']])
  })
})
