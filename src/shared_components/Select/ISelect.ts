import { PropValidator } from 'vue/types/options'

export interface ISelectOption {
    name: String,
    value: String
}

export interface ISelectProps {
    options: Array<ISelectOption>,
    onChangeEmitName: PropValidator<String>
}

export interface ISelectData {
    selectedValue: string
}
