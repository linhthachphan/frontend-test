import { shallowMount, mount } from '@vue/test-utils'
import { ISelectOption } from './ISelect'
import Select from './Select.vue'
const optionsMockData: Array<ISelectOption> = [
  {
    name: 'something',
    value: '1'
  },
  {
    name: 'else',
    value: '2'
  }]

describe('options props rendering', () => {
  it('should find 2 option element', () => {
    const wrapper = shallowMount(Select, {
      propsData: {
        options: optionsMockData
      }
    })
    expect(wrapper.findAll('option').length).toBe(2)
  })

  it('should find 0 option element', () => {
    const wrapper = shallowMount(Select, {
      propsData: {
        options: []
      }
    })
    expect(wrapper.findAll('option').length).toBe(0)
  })

  it('should match first options value', () => {
    const wrapper = shallowMount(Select, {
      propsData: {
        options: optionsMockData
      }
    })
    const optionValue = wrapper.find('option:first-child').attributes().value
    expect(optionValue).toBe(optionsMockData[0].value)
  })

  it('should match first options inner text', () => {
    const wrapper = shallowMount(Select, {
      propsData: {
        options: optionsMockData
      }
    })
    const optionName = wrapper.find('option:first-child').text()
    expect(optionName).toBe(optionsMockData[0].name)
  })
})

describe('selected value', () => {
  it('should match the selectedValue of default value', () => {
    const wrapper = mount(Select, {
      propsData: {
        options: optionsMockData,
        defaultValue: optionsMockData[1].value
      }
    })

    expect(
      wrapper.vm.$data.selectedValue
    ).toBe(optionsMockData[1].value)
  })

  it('should match the selectedValue after changing the option', async () => {
    const wrapper = mount(Select, {
      propsData: {
        options: optionsMockData,
        defaultValue: optionsMockData[0].value
      }
    })

    await wrapper.find('option:last-child').setSelected()
    expect(
      wrapper.vm.$data.selectedValue
    ).toBe(optionsMockData[optionsMockData.length - 1].value)
  })
})

describe('emmited events', () => {
  it('should emmit the selectionChanged event', async () => {
    const wrapper = mount(Select, {
      propsData: {
        options: optionsMockData,
        defaultValue: optionsMockData[0].value
      }
    })
    await wrapper.find('option:last-child').setSelected()
    expect(wrapper.emitted()['select-changed']).toBeDefined()
  })
})
