import Select from './Select.vue'
export default {
  title: 'Controls/Select',
  component: Select,
  argTypes: {
    options: {
      description: 'key value pair (name, value) that represets an option',
      table: {
        defaultValue: { summary: 'x' }
      }
    }
  }
}

const SelectTemplate = (args: any, { argTypes }: any) => ({
  components: { Select },
  props: Object.keys(argTypes),
  template: `
  <Select 
    :options="options"
    :onChangeEmitName="onChangeEmitName"  
    :defaultValue="defaultValue"
  />`
})

const Primary: any = SelectTemplate.bind({})
Primary.args = {
  options: [
    {
      name: 'option 1',
      value: 1
    }, {
      name: 'option 2',
      value: 2
    }
  ],
  defaultValue: 2
}

export {
  Primary
}
