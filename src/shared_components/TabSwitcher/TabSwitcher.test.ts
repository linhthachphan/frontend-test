import { shallowMount, mount } from '@vue/test-utils'
import TabSwitcher from './TabSwitcher.vue'
describe('tab switcher rendering', () => {
  it('should contain 3 tab selectors', () => {
    const component = shallowMount(
      TabSwitcher,
      {
        propsData: {
          numberOfTabs: 3,
          tabTitles: ['tab1', 'tab2', 'tab3']
        }
      })
    expect(component.findAll('.tab_switcher__selectors>div').length).toBe(3)
  })

  it('should attach the active class to the 3rd tab after click', async () => {
    const component = mount(
      TabSwitcher,
      {
        propsData: {
          numberOfTabs: 3,
          tabTitles: ['tab1', 'tab2', 'tab3']
        }
      })
    await component.find('.tab_switcher__selectors>div:last-child').trigger('click')
    expect(component.find('.tab_switcher__selectors>div:last-child').classes()).toContain('active')
  })

  it('should hide the first and third tab', async () => {
    const component = mount(
      TabSwitcher,
      {
        propsData: {
          numberOfTabs: 3,
          tabTitles: ['tab1', 'tab2', 'tab3']
        },
        slots: {
          tab1: '<div id="content1"></div>',
          tab2: '<div id="content2"></div>',
          tab3: '<div id="content3"></div>'
        }
      })
    await component.find('.tab_switcher__selectors>div:nth-child(2)').trigger('click')
    expect([component.find('#content1').isVisible(), component.find('#content1').isVisible()]).toEqual([false, false])
  })

  it('should display the second tab', async () => {
    const component = mount(
      TabSwitcher,
      {
        propsData: {
          numberOfTabs: 3,
          tabTitles: ['tab1', 'tab2', 'tab3']
        },
        slots: {
          tab1: '<div id="content1"></div>',
          tab2: '<div id="content2"></div>',
          tab3: '<div id="content3"></div>'
        }
      })
    await component.find('.tab_switcher__selectors>div:nth-child(2)').trigger('click')
    expect(component.find('#content2').isVisible()).toBe(true)
  })
})
