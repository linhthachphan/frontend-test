import TabSwitcher from './TabSwitcher.vue'

export default {
  title: 'Controls/TabSwitcher',
  argTypes: {
    numberOfTabs: {
      description: 'number of tabs',
      table: {
        defaultValue: { summary: 0 }
      }
    },
    tabTitles: {
      description: 'is the input non editable'
    }
  }
}
const TabSwitcherTemplate = (args: any, { argTypes }: any) => ({
  components: { TabSwitcher },
  props: Object.keys(argTypes),
  template: `
    <TabSwitcher :numberOfTabs="numberOfTabs" :tabTitles="tabTitles">
      <template #tab1>tab 1 content</template>
      <template #tab2>tab 2 content</template>
    </TabSwitcher>
  `
})

const Primary: any = TabSwitcherTemplate.bind({})

Primary.args = {
  numberOfTabs: 2,
  tabTitles: ['tab 1', 'tab2']
}

export {
  Primary
}
