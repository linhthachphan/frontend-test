/**
 * @module shims.d.ts contains definition
 * for .vue files
 */
declare module '*.vue' {
    import Vue from 'vue'
    export default Vue
}
