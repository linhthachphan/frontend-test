const CONVERSION_TYPES:Record<string, string> = {
  FIAT: 'Fiat',
  CRYPTO: 'Crypto'
}

/**
 * Definiton of a conversion type object
 * @interface
 */
export interface IConversionType {
    /**
     * @property {String} name - displayed to the user
     */
    name: String,
    /**
     * @property {String} value - used internally
     */
    value: typeof CONVERSION_TYPES
}

export default CONVERSION_TYPES
