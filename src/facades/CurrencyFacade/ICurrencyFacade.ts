export interface ICryptoCurrency {
    symbol: string
    price: string
}

export interface IFiatResponse {
    rates: {
        [key: string]: number
    },
    base: string,
    date: string
}
