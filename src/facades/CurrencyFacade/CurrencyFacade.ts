import axios, { AxiosError, AxiosResponse } from 'axios'
import { ICryptoCurrency, IFiatResponse } from './ICurrencyFacade'

enum CURRENCY_APIS {
    'FIDA' = 'https://api.exchangeratesapi.io/',
    'CRYPTO' = 'https://api.binance.com/api/v3/'
}

export default class CurrencyFacade {
  public static async getCryptoCurrencies (): Promise<Array<ICryptoCurrency> | null> {
    const results: AxiosResponse<Array<ICryptoCurrency>> = await axios.get(CURRENCY_APIS.CRYPTO + 'ticker/price')
    if (results.status === 200) {
      return results.data
    } else {
      return null
    }
  }

  public static async getFiatCurrencies (): Promise<Array<string>|null> {
    const cache: Cache = await caches.open('currency-cache')
    if ((await cache.match(CURRENCY_APIS.FIDA + 'latest')) === undefined) {
      await cache.add(CURRENCY_APIS.FIDA + 'latest')
    }
    const results: any = await axios.get(CURRENCY_APIS.FIDA + 'latest')
    console.log(results)
    if (results.status === 200) {
      return [...Object.keys(results.data.rates), results.data.base]
    } else {
      return null
    }
  }

  public static async getFiatAmount (
    startCurrencySymbol: string,
    targetCurrencySymbol: string,
    value: number
  ):Promise<number|null> {
    const apiString: string =
      CURRENCY_APIS.FIDA + 'latest?base=' + startCurrencySymbol +
      '&symbols=' + targetCurrencySymbol

    const cache: Cache = await caches.open('currency-cache')
    if ((await cache.match(apiString)) === undefined) {
      await cache.add(apiString)
    }
    return await axios.get(apiString).then(
      (result: AxiosResponse<IFiatResponse>):number =>
        result.data.rates[targetCurrencySymbol] * value
    ).catch((e: AxiosError) => {
      console.warn(e.message)
      return null
    })
  }
}
