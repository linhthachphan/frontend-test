# Opeepl Frontend Task
## Build with:
- Vue 2.6
- Webpack 4.44
- Jest 26.6
- Storybook 6.1
- Typescript 4.1
## Commands:
-    "npm start": "webpack serve" - development
-   "npm build": "webpack" - build 
-   "npm test": "jest" - run jest tests
-   "npm lint": "eslint 'src/**'" - run lint
-   "npm" storybook": "start-storybook -p 6006" - storybook documentation
    